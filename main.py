from kivy.lang import Builder
from back_end.kivy_class.KivyClass import MainApp

import glob

if __name__ == '__main__':
    kivy_files = glob.glob("front_end/widget_design/*kv")

    for file in kivy_files:
        Builder.load_file(file)
    MainApp().run()