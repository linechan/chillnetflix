import psycopg2
import json

class Database:
    def __init__(self):
        # Read info to access database
        with open("./back_end/database_credential.json") as file:
            credential = json.load(file)

        # Create a conngictor to the database
        self.host = credential["host"]
        self.database = credential["database"]
        self.user = credential["user"]
        self.password = credential["password"]
        self.conn = None
        self.cursor = None

    def __del__(self):
        pass

    def connect(self):
        # Create a connector to the database
        self.conn = psycopg2.connect(host=self.host,
                                     database=self.database,
                                     user=self.user,
                                     password=self.password)

        self.cursor = self.conn.cursor()

    def close(self):
        # Commit changes
        self.conn.commit()
        # Close connexion
        self.cursor.close()
        self.conn.close()
