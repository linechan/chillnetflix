from back_end.database import Database
import psycopg2

class UserVideoDatabase(Database):
    # Videos rating
    # 0 : don't want to watch
    # 1 : Already watched but you would redo
    # 2 : Want to watch

    def __init__(self, id):
        super().__init__()
        self.id = id
        self.connect()
        # Create a users table if doesn't exist
        self.user_videos_table = "user_videos_" + str(id)
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS %s (netflixid INT PRIMARY KEY, rating INT NOT NULL)" % (
            self.user_videos_table,))
        self.close()

    def find_unrated_videos(self):
        self.connect()
        user_netflixid = self.user_videos_table + ".netflixid"
        query = "SELECT * FROM videos FULL OUTER JOIN %s USING(netflixid) WHERE %s IS NULL" % (self.user_videos_table, user_netflixid)
        self.cursor.execute(query)
        videos_list = self.cursor.fetchall()
        self.close()
        return videos_list

    def insert_video(self, netflixid, rating):
        try:
            self.connect()
            query = "INSERT INTO %s VALUES(%s, %s)" % (self.user_videos_table, netflixid, rating)
            self.cursor.execute(query)
            self.close()
        except psycopg2.errors.UniqueViolation:
            pass

    def read_user_video(self):
        self.connect()
        query = "SELECT * FROM %s" % self.user_videos_table
        self.cursor.execute(query)
        user_videos = self.cursor.fetchall()
        self.close()
        return user_videos

    def find_video(self):
        self.connect()
        query = "SELECT * FROM videos"
        self.cursor.execute(query)
        videos = self.cursor.fetchall()
        self.close()
        return videos

    # Count the number of videos in the list
    def count(self):
        self.connect()
        query = "SELECT COUNT(*) from %s" % self.user_videos_table
        self.cursor.execute(query)
        video_nb = self.cursor.fetchall()[0][0]
        self.close()
        return video_nb

    def find_common_vid(self, user_id1, user_id2):
        self.connect()
        table1 = "user_videos_" + str(user_id1)
        table1_netflixid = table1 + ".netflixid"
        table1_rating = table1 + ".rating"
        table2 = "user_videos_" + str(user_id2)
        table2_netflixid = table2 + ".netflixid"
        table2_rating = table2 + ".rating"

        query = "SELECT %s FROM %s FULL OUTER JOIN %s USING(netflixid) WHERE %s=%s AND %s!=0 AND %s != 0" %\
                                (table1_netflixid,
                                table1,
                                table2,
                                table1_netflixid,
                                table2_netflixid, table1_rating, table2_rating)
        query2 = "SELECT * FROM videos JOIN(" + query + ") ret ON (ret.netflixid = videos.netflixid)"
        self.cursor.execute(query2)
        matched_vid = self.cursor.fetchall()
        return matched_vid
        self.close()
