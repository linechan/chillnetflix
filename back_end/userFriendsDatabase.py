from back_end.database import Database

class UserFriendsDatabase(Database):
    def __init__(self, id):
        super().__init__()
        self.user_friends_table = "user_friends_" + str(id)
        self.connect()
        # Create a users table if doesn't exist
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS %s (id INT PRIMARY KEY)" % (self.user_friends_table,))
        self.close()

    def insert_friend(self, id):
        try:
            self.connect()
            query = "INSERT INTO %s VALUES (%d)" % (self.user_friends_table, id)
            self.cursor.execute(query)
            self.close()
            return True
        except:
            self.close()
            return False

    def read(self):
        self.connect()
        query = "SELECT * FROM %s" % self.user_friends_table
        self.cursor.execute(query)
        friends = self.cursor.fetchall()
        self.close()
        return friends

    def find_match(self, id):
        friends = self.read()
        match_list = []
        self.connect()
        for friend in friends:
            friend_vid_table = "user_videos_" + str(friend[0])
            query = "SELECT * FROM %s  WHERE %s=%s AND %s=2 OR %s=1" % (friend_vid_table,
                                                                        friend_vid_table + ".netflixid",
                                                                        int(id),
                                                                        friend_vid_table+ ".rating",
                                                                        friend_vid_table+ ".rating")
            self.cursor.execute(query)
            video = self.cursor.fetchall()
            if len(video) :
                match_list.append(friend[0])
        self.cursor.close()
        return match_list

    # Returns friends count
    def count(self):
        self.connect()
        query = "SELECT COUNT(*) from %s" % self.user_friends_table
        self.cursor.execute(query)
        friend_nb = self.cursor.fetchall()[0][0]
        self.close()
        return friend_nb