from back_end.userVideoDatabase import UserVideoDatabase
from back_end.userFriendsDatabase import UserFriendsDatabase
from back_end.usersDatabase import UsersDatabase


class User:
    def __init__(self, id):
        self.id = id
        # Create a user's rated videos list
        self.videos_database = UserVideoDatabase(id)
        # Create a user's friend list
        self.friends_database = UserFriendsDatabase(id)

        # Fetch user info in user database
        users_database = UsersDatabase()
        user_data = users_database.search_user_by_id(id)[0]
        self.login = user_data[1]
        self.mail = user_data[2]
        self.video_match_lst = []
        self.current_video = None

    def add_friend(self, id):
        return self.friends_database.insert_friend(id)

    def rate_current_video(self, rating):
        self.videos_database.insert_video(self.current_video[0], rating)

    # Set current video
    def find_video_to_rate(self):
        # If the video match list is empty, fetch new unrated videos
        if len(self.video_match_lst) == 0:
            self.video_match_lst = self.videos_database.find_unrated_videos()

        # If the user already matched all videos
        if len(self.video_match_lst) == 0:
            self.current_video = None
        else:
            # Otherwise set current video and remove it from matching list
            self.current_video = self.video_match_lst.pop()

    # Return video title
    def get_curr_video_title(self):
        return self.current_video[1]

    # Return video image
    def get_curr_video_img(self):
        return self.current_video[2]

    # Return video synopsis
    def get_curr_video_synopsis(self):
        return self.current_video[3]

    # Return release date
    def get_curr_video_release(self):
        return self.current_video[4]

    # Return visssdeo runtime
    def get_curr_video_runtime(self):
        return self.current_video[5]

    # Return list of friends ID who matched with the user
    def find_match(self):
        matched_friends = self.friends_database.find_match(self.current_video[0])
        return matched_friends

    # Return user's friend nb
    def count_friends(self):
        return self.friends_database.count()

    # Return nb of rated videos
    def count_videos(self):
        return self.videos_database.count()

    # Returns user's friend list
    def get_friend_list(self):
        friends = self.friends_database.read()
        friend_list = []
        # Fetch user login
        user_database = UsersDatabase()
        for friend in friends:
            friend_list += user_database.search_user_by_id(friend[0])
        return friend_list

    # Return videos matched with friend's id
    def get_video_match_with_friend(self, friend_id):
        return self.videos_database.find_common_vid(self.id, friend_id)

