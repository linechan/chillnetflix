from kivy.uix.screenmanager import Screen
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label

# Friend list class
class FriendListScreen(Screen):
    # Loads friend list and display
    def on_pre_enter(self, *args):
        user = App.get_running_app().user
        friend_list = user.get_friend_list()
        self.ids.title.text = "You have " + str(len(friend_list)) + " friends:"
        self.ids.scroll_view.add_widget(self.create_scrollview(friend_list))

    # Clear scrollview widget when the page is removed
    def on_leave(self, *args):
        self.ids.scroll_view.clear_widgets()

    # Display friend info
    def create_scrollview(self, friend_list):
        grid_layout = GridLayout(cols=1, size_hint_y=None)
        # For each friend, display videos you both want to watch
        for friend in friend_list:
            profile_btn = Button(text=friend[1], size_hint_y=None)
            vid_match_list = App.get_running_app().user.get_video_match_with_friend(friend[0])
            # Only display first 4 movies
            label_text = ""
            for vid in vid_match_list[:4]:
                label_text += vid[1] + ", "
            title_label = Label(text=label_text, size_hint_y=None)

            # Create a row for each friend
            box_layout = BoxLayout(orientation="horizontal", size_hint_y = None)
            box_layout.add_widget(profile_btn)
            box_layout.add_widget(title_label)
            # Add it to main layout (ScrollView's widget)
            grid_layout.add_widget(box_layout)

        return grid_layout

    # Change screen to home page
    def goto_profile(self):
        self.manager.transition.direction = "down"
        self.manager.current = "profile_screen"
