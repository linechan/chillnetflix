from kivy.uix.screenmanager import Screen
from kivy.app import App

# Profile page
class ProfileScreen(Screen):
    # Load user info
    def on_pre_enter(self, *args):
        user = App.get_running_app().user
        self.ids.user_name.text = user.login
        self.ids.user_mail.text = user.mail

    # Change screen to home page
    def goto_home(self):
        self.manager.transition.direction = "left"
        self.manager.current = "home_screen"

    # Change screen to friend list page
    def goto_friend_list(self):
        self.manager.transition.direction = "up"
        self.manager.current = "friend_list_screen"

    # Reset user and change screen to log in
    def log_out(self):
        App.get_running_app().user = None
        self.manager.transition.direction = "right"
        self.manager.current = "login_screen"

    # Edit user personal data
    def edit_info(self):
        self.manager.transition.direction = "left"
        self.manager.current = "edit_profile_screen"