from kivy.uix.screenmanager import Screen
from kivy.app import App
from kivy.uix.popup import Popup

from back_end.usersDatabase import UsersDatabase


# Edit popup
class EditProfilePopup(Popup):
    pass


#Edit profile page
class EditProfileScreen(Screen):
    # Show user data
    def on_pre_enter(self, *args):
        user = App.get_running_app().user
        self.ids.login.hint_text = user.login
        self.ids.mail.hint_text = user.mail

    # Delete inputs when the page is removed
    def on_leave(self, *args):
        self.ids.login.text = ""
        self.ids.mail.text = ""
        self.ids.pwd.text = ""
    # Log out
    def log_out(self):
        App.get_running_app().user = None
        self.manager.transition.direction = "right"
        self.manager.current = "login_screen"

    # Change screen to user profile
    def goto_profile(self):
        self.manager.transition.direction = "right"
        self.manager.current = "profile_screen"

    # Fetch user inputs and update database
    def save(self, login, mail, pwd):
        users_database = UsersDatabase()
        user = App.get_running_app().user
        if login != "":
            users_database.update_user_login(user.id, login)
            user.login = login
        if mail != "":
            users_database.update_user_mail(user.id, mail)
            user.mail = mail
        if pwd != "":
            users_database.update_user_pwd(user.id, pwd)

        # Display a success popup
        EditProfilePopup().open()
        # Go back to user profile
        self.manager.transition.direction = "right"
        self.manager.current = "profile_screen"
