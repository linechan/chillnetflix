from kivy.uix.screenmanager import ScreenManager
from kivy.app import App

from kivy.uix.image import Image
from kivy.uix.behaviors import ButtonBehavior
from back_end.kivy_class.hoverable import HoverBehavior

# Import all kivy classes
from back_end.kivy_class.addFriendScreen import AddFriendScreen
from back_end.kivy_class.homeScreen import HomeScreen
from back_end.kivy_class.loginScreen import LoginScreen
from back_end.kivy_class.profileScreen import ProfileScreen
from back_end.kivy_class.signUpScreen import SignUpScreen
from back_end.kivy_class.signUpSuccessScreen import SignUpSuccessScreen
from back_end.kivy_class.videoMatchingScreen import VideoMatchingScreen
from back_end.kivy_class.editProfileScreen import EditProfileScreen
from back_end.kivy_class.friendListScreen import FriendListScreen
from back_end.kivy_class.selectWatchingScreen import SelectWatchingScreen

class RootWidget(ScreenManager):
    pass


# Kivy main application
class MainApp(App):
    user = None

    # app = App.get_running_app()
    # print(app.user)
    def build(self):
        return RootWidget()


# Define hover behavior on image button
# Inherit Button Behaviour before Image. Otherwise the image cover button functionalities
class ImageButton(ButtonBehavior, HoverBehavior, Image):
    pass
