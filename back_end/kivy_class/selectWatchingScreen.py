from kivy.uix.screenmanager import Screen

class SelectWatchingScreen(Screen):
    def goto_home(self):
        self.manager.transition.direction = "left"
        self.manager.current = "home_screen"