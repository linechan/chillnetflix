from kivy.uix.screenmanager import Screen
from back_end.usersDatabase import UsersDatabase


# Sign up page
class SignUpScreen(Screen):
    # Clear inputs when the screen is removed
    def on_leave(self, *args):
        self.ids.username.text = ""
        self.ids.password.text = ""
        self.ids.mail.text = ""

    # Submit user form
    def submit(self, uname, umail, upwd):
        # Check for empty values
        if uname == "":
            self.ids.validation.text = "You must enter a login name"
            return
        elif umail == "":
            self.ids.validation.text = "You must enter a mail address"
            return
        elif upwd == "":
            self.ids.validation.text = "Enter a password"
            return

        database = UsersDatabase()

        # Add new user to database
        if database.insert_user(uname, umail, upwd):
            self.manager.current = "login_success_screen"
        else:
            self.ids.validation.text = "Login not available"

    # Change screen to login page
    def togo_login(self):
        self.manager.transition.direction = "right"
        self.manager.current = "login_screen"
