from kivy.uix.screenmanager import Screen


# Screen showed when user signed in successfully
class SignUpSuccessScreen(Screen):
    def goto_login(self):
        self.manager.transition.direction = "right"
        self.manager.current = "login_screen"
