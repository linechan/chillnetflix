from kivy.uix.screenmanager import Screen
from kivy.app import App
from back_end.usersDatabase import UsersDatabase
from back_end.user import User


# Add a friend
class AddFriendScreen(Screen):
    # Clear inputs when the screen is removed
    def on_leave(self, *args):
        self.ids.friend_login.text = ""
        self.ids.friend_result.text = ""

    # Change screen : home page
    def go_home(self):
        self.manager.transition.direction = "right"
        self.manager.current = "home_screen"

    # Log out
    def log_out(self):
        App.get_running_app().user = None
        self.manager.transition.direction = "right"
        self.manager.current = "login_screen"

    # Search friends' list
    def search_friend(self, login):
        database = UsersDatabase()
        friend = database.search_user_by_login(login)
        if not friend:
            self.ids.friend_result.text = "Oops ! This user doesn't exist"
            return

        # Fetch user data and add a friend
        app = App.get_running_app()
        user = app.user
        # Check if friend not already added
        if not user.add_friend(friend[0][0]):
            self.ids.friend_result.text = "You're already friends :)"
            return

        # Add user in its friend list
        friend_user = User(friend[0][0])
        friend_user.add_friend(user.id)
        self.ids.friend_result.text = "You're friend with %s now !" % friend_user.login
