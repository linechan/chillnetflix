from kivy.uix.screenmanager import Screen
from kivy.app import App
from back_end.usersDatabase import UsersDatabase

from kivy.uix.popup import Popup


# Matching pop up
class MatchingPopup(Popup):
    pass


# Video matching page
class VideoMatchingScreen(Screen):
    # Called when the screen is loaded
    def on_pre_enter(self, *args):
        user = App.get_running_app().user
        user.find_video_to_rate()
        self.display_video_to_rate()

    # Change screen to homepage
    def goto_home(self):
        self.manager.transition.direction = "up"
        self.manager.current = "home_screen"

    # Show a video
    def display_video_to_rate(self):
        user = App.get_running_app().user
        if user.current_video:
            self.ids.video_img.source = user.get_curr_video_img()
            self.ids.synopsis.text = user.get_curr_video_synopsis()
            self.ids.video_title.text = user.get_curr_video_title()
            self.ids.release_date.text = user.get_curr_video_release()
            self.ids.runtime.text = user.get_curr_video_runtime()
        else:
            self.ids.release_date.text = ""
            self.ids.runtime.text = ""
            self.ids.video_title.text = "Oops\nYou rated all available videos"
            self.ids.video_img.source = "front_end/icons/prohibited.jpg"
            self.ids.synopsis.text = ""

    # Add in user video list with rating "Don't want to watch"
    def rate_no(self):
        user = App.get_running_app().user
        if user.current_video:
            user.rate_current_video(0)
            user.find_video_to_rate()
            self.display_video_to_rate()

    # Add in user video list with rating "Would rewatch"
    def rate_would_rewatch(self):
        user = App.get_running_app().user
        if user.current_video:
            user.rate_current_video(1)
            user.find_video_to_rate()
            self.display_video_to_rate()

    # Add in user video list with rating "Want to watch it"
    def rate_yes(self):
        user = App.get_running_app().user
        # If there is a video to rate
        if user.current_video:
            user.rate_current_video(2)
            self.find_match()
            # Find a match is the user rated "YES"
            user.find_video_to_rate()
            self.display_video_to_rate()

    # Search if a friend wants to watch this video
    def find_match(self):
        user = App.get_running_app().user
        # List of users' ID
        matched_friend = user.find_match()
        # Exit if no match
        if not len(matched_friend):
            return

        popup_title = "You matched with "
        users_database = UsersDatabase()
        for friend in matched_friend:
            popup_title += users_database.search_user_by_id(friend)[0][1]
            if friend != matched_friend[-1]:
                popup_title += ", "

        # Display a matching popup
        match_popup = MatchingPopup()
        match_popup.title = popup_title
        match_popup.open()
