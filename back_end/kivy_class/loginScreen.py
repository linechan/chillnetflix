from kivy.uix.screenmanager import Screen
from kivy.app import App
from back_end.usersDatabase import UsersDatabase
from back_end.user import User


# Login screen : 1st screen the user sees
class LoginScreen(Screen):
    # Clear inputs when the screen is removed
    def on_leave(self, *args):
        self.ids.username.text = ""
        self.ids.password.text = ""

    # Call Sign up Page
    def goto_sign_up(self):
        self.manager.transition.direction = "left"
        self.manager.current = "signup_screen"

    # Log in
    def login(self, uname, upwd):
        self.ids.login_wrong.text = ""
        # Check username and password in user database
        database = UsersDatabase()
        # Create database if doesn't exist
        database.create()
        user_data = database.check_login_pwd(uname, upwd)

        # Show error on screen if user not found
        if not user_data:
            self.ids.login_wrong.text = "Wrong login or password"
            return

        # Configure user (fetch Application attributes)
        app = App.get_running_app()
        app.user = User(user_data[0][0])

        # Load home page
        self.manager.transition.direction = "left"
        self.manager.current = "home_screen"
