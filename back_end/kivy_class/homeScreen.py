from kivy.uix.screenmanager import Screen
from kivy.app import App


# Home page
class HomeScreen(Screen):
    # Called when screen is loaded
    def on_pre_enter(self, *args):
        app = App.get_running_app()
        self.ids.welcome_label.text = "Welcome home " + app.user.login

    # Log out
    def log_out(self):
        self.manager.transition.direction = "right"
        App.get_running_app().user = None
        self.manager.current = "login_screen"

    # Change screen : show user's profile
    def goto_profile(self):
        self.manager.transition.direction = "right"
        self.manager.current = "profile_screen"

    # Add a new friend
    def goto_addfriend(self):
        self.manager.transition.direction = "left"
        self.manager.current = "add_friend_screen"

    # Change screen : to go video matching
    def goto_videomatching(self):
        self.manager.transition.direction = "up"
        self.manager.current = "video_matching_screen"

    # Change screen : select matching
    def goto_select_watching(self):
        self.manager.transition.direction = "right"
        self.manager.current = "select_watching_screen"
