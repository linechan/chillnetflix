from back_end.database import Database

class UsersDatabase(Database):

    def __init__(self):
        super().__init__()

    def create(self):
        self.connect()
        # Create a users table if doesn't exist
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS users (id SERIAL PRIMARY KEY ,login TEXT NOT NULL, mail TEXT NOT NULL, pwd TEXT NOT NULL)")
        self.close()

    # Returns a list of user tuple
    def read_users(self):
        self.connect()
        self.cursor.execute("SELECT * FROM users")
        users = self.cursor.fetchall()
        self.close()
        return users

    # Add a new user to database
    def insert_user(self, login, mail, pwd):
        # Check if login is available
        user = self.search_user_by_login(login)

        if user:
            return False

        self.connect()
        # Add new user in users list
        self.cursor.execute("INSERT INTO users VALUES (DEFAULT, %s, %s, %s)", (login, mail, pwd))
        self.close()
        return True

    # Delete a user by login
    def delete(self, login):
        self.connect()
        self.cursor.execute("DELETE FROM users WHERE login=%s", (login,))
        self.close()

    # Update user login
    def update_user_login(self, id, new_login):
        self.connect()
        self.cursor.execute("UPDATE users SET login=%s WHERE id=%s", (new_login, id))
        self.close()

    # Update user mail
    def update_user_mail(self, id, new_mail):
        self.connect()
        self.cursor.execute("UPDATE users SET mail=%s WHERE id=%s", (new_mail, id))
        self.close()

    # Update user password
    def update_user_pwd(self, id, new_pwd):
        self.connect()
        self.cursor.execute("UPDATE users SET pwd=%s WHERE id=%s", (new_pwd, id))
        self.close()

    def check_login_pwd(self, login, pwd):
        self.connect()
        self.cursor.execute("SELECT * FROM users WHERE login=%s AND pwd=%s", (login,pwd))
        user_found = self.cursor.fetchall()
        self.close()
        return user_found

    # Find user info by login
    def search_user_by_login(self, login):
        self.connect()
        # Check if login is available
        self.cursor.execute("SELECT * FROM users WHERE login=%s", (login,))
        user = self.cursor.fetchall()
        self.close()
        return user

    # Find user by ID
    def search_user_by_id(self, id):
        self.connect()
        self.cursor.execute("SELECT * FROM users WHERE id=%s", (id, ))
        user = self.cursor.fetchall()
        self.close()
        return user




